/**
 * Created by Konrad on 2015-02-21.
 */
(function() {
    var game; //Only one global object, hope so

    window.onload = function () {
        game = new Phaser.Game(480, 640, Phaser.CANVAS, "gameWindow");
        game.state.add('menu', new MenuState(game));
        game.state.add('game', new GameState(game));
        game.state.start('menu');

    }


})();