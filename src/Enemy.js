/**
 * Created by Konrad on 2015-02-28.
 */
function Enemy(game) {
    this.game = game;
    this.chanceToGenerate = 990;
    //TODO: Przemyśleć czy to wsadzić do osobnej klasy
    //TODO: Czy potrzebny jest nam jeszcze parametr dotyczący losowania?
    this.enemyProperty = {

    };
    this.enemyGroup = null;
    this.enemyNonMap = ["chocolate", "kebab", "beer", "steroids", "fat"]; //Dziwne, wiem
}

Enemy.prototype.preload = function() {
    this.game.load.image("chocolate", "assets/img/enemy/czekolada.png");
    this.game.load.image("kebab", "assets/img/enemy/kebab.png");
    this.game.load.image("beer", "assets/img/enemy/piwko.png");
    this.game.load.image("steroids", "assets/img/enemy/strzykawka.png");
    this.game.load.image("fat", "assets/img/enemy/tluszcz.png");
    this.enemyGroup = this.game.add.physicsGroup();
};


Enemy.prototype.update = function() {
    var chance = Math.floor(Math.random() * 1000 + 1);

    var doWeGenerate = (chance > this.chanceToGenerate);

    if(doWeGenerate) {
        //console.log(chance);
        var enemyChoice = Math.floor(Math.random() * 5); //Ta '5' burzy zasady rozwijalnego oprogramowania
        var enemyName = this.enemyNonMap[enemyChoice];
        //Make a sprite
        //TODO: Zadecydować czy poniższe przenieść do osobnej metody
        //TODO: Zrobić tak by nie było zahardkodowane
        var x = Math.floor(Math.random() * 450  + 10) //Zahardkodowane fest!

        //var enemySprite = this.game.add.sprite(x, 0, enemyName);
        var enemySprite = this.enemyGroup.create(x, 0, enemyName);
        enemySprite.checkWorldBounds = true;
        this.game.physics.arcade.enable(enemySprite);
        enemySprite.outOfBoundsKill = true;
        enemySprite.body.velocity.y = 300; //Zahardkodowane
    }
}