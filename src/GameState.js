/**
 * Created by Konrad on 2015-02-21.
 */
function GameState(game) {
    this.game = game;
    this.player = new Player(game);
    this.powerUpManager = new PowerUpManager(game);
    this.enemy = new Enemy(game);

    //TODO: Sprawdzić czy angielski jest tu dobry
    //TODO: Sprawdzić czy można zapisać tak poniższe i wsadzić tam naszą wartość
    this.endText = "You lose. You have gained {points} points.";
    this.endTextProperties = {
        font: '20px Arial',
        fill: '#fff',
        align: 'left'
    };
}

GameState.prototype.preload = function() {
    //FOR TESTING
    //TODO: Zrobić normalne menu
    //TODO: Pasek ładowania też można
    this.game.load.image('background', 'assets/img/background/tlo.png');
    this.game.load.image('floor', 'assets/img/background/podloga.png');
    this.player.preload();
    this.powerUpManager.preload();
    this.enemy.preload();
};

GameState.prototype.create = function() {
    this.player.create();
};

GameState.prototype.update = function() {
    this.player.update();
    this.checkPlayerCollisions();
    this.powerUpManager.update();
    this.enemy.update();
};

GameState.prototype.checkPlayerCollisions = function() {
    //Player and enemy
    this.game.physics.arcade.collide(this.player.playerSprite, this.enemy.enemyGroup, this.playerLostLive, null, this);
    //Player and weapon

    //Player and powerup
    this.game.physics.arcade.collide(this.player.playerSprite, this.powerUpManager.powerUpGroup, this.playerAddPoints, null, this);
    //We still live?
    if(this.player.died) {
        //Zmień stan gry
    }
};

GameState.prototype.playerLostLive = function(player, enemy) {
    this.player.lostLive();
    enemy.kill();
};

GameState.prototype.playerAddPoints = function(player, powerUp) {
    this.player.addPoints(this.powerUpManager.getPoints(powerUp.key));
    powerUp.kill();
};

GameState.prototype.endGame = function() {
    this.player.playerSprite.kill();
    //TODO: delete w JS


}