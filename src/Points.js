function Points(game, x, y, pointsValue, livesValue) {
	this.game = game;
	this.x = x;
	this.y = y;

	this.pointsValue = pointsValue;
    //TODO: Sprawdzić czy nie lepiej dać obrazek zamiast zwykłego tekstu
    this.pointsText = "Points = ";
    this.pointsTextObject = null
    this.pointsTextProperties = {
        font: '20px Arial',
        fill: '#fff',
        align: 'left'
    };

    this.livesValue = livesValue;
    this.livesText = "Lives = ";
    this.livesTextProperties = {
        font: '20px Arial',
        fill: '#fff',
        align: 'right'
    };
    this.livesTextObject = null;

//TODO: właściwości czcionek
}

Points.prototype.create = function() {
    //TODO: MAJĄ BYC ZMIENNE, BEZ HARDKODOWANIA!
    this.pointsTextObject = this.game.add.text(this.x, this.y, this.pointsText + this.pointsValue.toString(), this.pointsTextProperties);
    this.livesTextObject = this.game.add.text(this.x + 300, this.y, this.livesText + this.livesValue.toString(), this.livesTextProperties);
};

Points.prototype.update = function(pointsValue, livesValue) {
    this.pointsValue = pointsValue;
    this.livesValue = livesValue;
    this.pointsTextObject.setText(this.pointsText + this.pointsValue.toString());
    this.livesTextObject.setText(this.livesText + this.livesValue.toString());
    //TODO: dodać jakichś słuchaczy czy coś
}