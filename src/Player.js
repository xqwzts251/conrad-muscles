/**
 * Created by Konrad on 2015-02-21.
 */
function Player(game) {
    this.game = game;
    this.level = 1;
    this.pointsValue = 0;
    this.lives = 3; //TODO: SPrawdzić czy to jest lepsze, czy może osobna klasa na życia, albo może livesImageClass
    this.points = new Points(this.game, 0 , 0, 0, this.lives);
    this.x = 15;
    this.y = 250;
    this.playerSprite = null;
    //Do we live?
    this.died = false;

    //ARROWS
    this.arrowLeft = null;
    this.arrowRight = null;

    //Left or right?
    this.isLeft = true;
    //CONSTANTS
    this.LEFT_BORDER = 0;
    this.RIGHT_BORDER = 480;
    this.PLAYER_WIDTH = 48;
    this.PLAYER_HEIGHT = 72;
    this.X_STEP = 5;
    this.weapon = new Weapon(this.game, this.x, this.y, this.PLAYER_WIDTH, this.PLAYER_HEIGHT); //!

    //Póki nie znajdziemy czegoś lepszego
    this.wasPressed = false;
};

Player.prototype.create = function() {
    this.arrowLeft = this.game.add.sprite(10, 300, 'arrowLeft');
    this.arrowRight = this.game.add.sprite(300, 300, 'arrowRight');
    this.arrowLeft.inputEnabled = true;
    this.arrowRight.inputEnabled = true;

    this.game.physics.startSystem(Phaser.Physics.ARCADE);
   // this.arrowLeft = this.game.add.button(10, 300, 'arrowLeft');
    //this.arrowRight = this.game.add.button(60, 300, 'arrowRight');
    this.playerSprite = this.game.add.sprite(this.x, this.y, 'playerLeft'); //TODO: add other direction as spritesheet or something else
    this.game.physics.arcade.enable(this.playerSprite);
    this.playerSprite.collideWorldBounds = true; //?
    this.playerSprite.checkWorldBounds = true; //?
    this.playerSprite.body.immovable = true;
    this.weapon.create();
    this.points.create();
};

Player.prototype.update = function() {
    this.checkControl();
    this.checkLives();
    
    this.weapon.update(this.x, this.y, this.isLeft);
    this.points.update(this.pointsValue, this.lives);

};

Player.prototype.preload = function() {
    this.game.load.image("playerLeft", "assets/img/player/adeptL.png");
    this.game.load.image("playerRight", "assets/img/player/adeptP.png");

    this.game.load.image("arrowLeft", "assets/img/arrows/arrowLeft.png");
    this.game.load.image("arrowRight", "assets/img/arrows/arrowRight.png");

    this.weapon.preload();
};

Player.prototype.checkControl = function() {
    //Keyboard & mouse
    //left
    if((this.game.input.keyboard.isDown(37) || this.arrowLeft.input.pointerDown(this.game.input.activePointer.id)) && this.playerSprite.body.x > this.LEFT_BORDER) {
        this.playerSprite.body.x -= this.X_STEP;
        this.isLeft = true;
    }
    else if((this.game.input.keyboard.isDown(39) || this.arrowRight.input.pointerDown(this.game.input.activePointer.id) )&& this.playerSprite.body.x < this.RIGHT_BORDER - this.PLAYER_WIDTH) {
        this.playerSprite.body.x += this.X_STEP;
        this.isLeft = false;
    }
    this.x = this.playerSprite.body.x;

    //TODO:Inny sposób na kliknięcia
    //HINT: sprawdzić czy równocześnie nie wciskamy strzałki
    var isArrowClicked = this.arrowLeft.input.pointerDown(this.game.input.activePointer.id) || this.arrowRight.input.pointerDown(this.game.input.activePointer.id);
    if(this.game.input.activePointer.isDown && !this.wasPressed && !isArrowClicked) //Jakoś działa, póki co - następnym razem można dać jakiegoś prostokąta "pod"
    {
        //console.log("Clicked!");
        this.weapon.fire();
        this.wasPressed = true
    }
    else if(this.wasPressed && !this.game.input.activePointer.isDown)
        this.wasPressed = false;

};

Player.prototype.checkLives = function () {
    if(this.lives <= 0)
        this.died = true;
}

Player.prototype.addPoints = function(value) {
    this.pointsValue += value;
}

Player.prototype.lostLive = function() {
    this.lives--;
}