/**
 * Created by Konrad on 2015-02-28.
 */
function PowerUpManager(game) {
    this.game = game;
    this.chanceToGenerate = 990;
    //TODO: Przemyśleć czy to wsadzić do osobnej klasy
    //TODO: Czy potrzebny jest nam jeszcze parametr dotyczący losowania?
    this.powerUpGroup = null;
    this.powerUpProperty = {
        "chicken" : {
            points: 40
        },
        "dumbbell" : { //hantle
            points: 100
        },
        "sandwich" : {
            points: 20
        },
        "milk" : {
            points: 30
        },
        "supplements" : {
            points: 50
        }
    }

    this.powerUpNonMap = ["chicken", "dumbbell", "sandwich", "milk", "supplements"]; //Dziwne, wiem
}

PowerUpManager.prototype.preload = function() {
    this.game.load.image("chicken", "assets/img/power_up/kurczak.png");
    this.game.load.image("dumbbell", "assets/img/power_up/hantle.png");
    this.game.load.image("sandwich", "assets/img/power_up/kanapeczka.png");
    this.game.load.image("milk", "assets/img/power_up/mleko.png");
    this.game.load.image("supplements", "assets/img/power_up/suple.png");

    this.powerUpGroup = this.game.add.physicsGroup();
};

PowerUpManager.prototype.update = function() {
    var chance = Math.floor(Math.random() * 1000 + 1);

    var doWeGenerate = (chance > this.chanceToGenerate);

    if(doWeGenerate) {
        //console.log(chance);
        var powerUpChoice = Math.floor(Math.random() * 5); //Ta '5' burzy zasady rozwijalnego oprogramowania
        var powerUpName = this.powerUpNonMap[powerUpChoice];
        //Make a sprite
        //TODO: Zadecydować czy poniższe przenieść do osobnej metody
        //TODO: Zrobić tak by nie było zahardkodowane
        var x = Math.floor(Math.random() * 450  + 10) //Zahardkodowane fest!
        //var powerUpSprite = this.game.add.sprite(x, 0, powerUpName);
        var powerUpSprite = this.powerUpGroup.create(x, 0, powerUpName);
        powerUpSprite.checkWorldBounds = true;
        this.game.physics.arcade.enable(powerUpSprite);
        powerUpSprite.outOfBoundsKill = true;
        powerUpSprite.body.velocity.y = 300; //Zahardkodowane
    }
}

PowerUpManager.prototype.getPoints = function(name) {
    return this.powerUpProperty[name].points;
}