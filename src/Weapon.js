/**
 * Created by Konrad on 2015-02-21.
 */
/*
* playerXLeft, playerYLeft... - our play could moves in one dimension, and we want to know at which coordinates
* we could draw weapon
 */
function Weapon(game, playerX, playerY, playerWidth, playerHeight ) {
    this.game = game;
    this.choice = 0;
    //this.ammo = 100;
    this.playerWidth = playerWidth;
    this.playerHeight = playerHeight;
    //For the beginning
    this.x = playerX;
    this.y = playerY;

    this.bullet = new Bullet(this.game);
    this.weaponSprite = null;
    this.weaponProperty = {
        "pistol": {
            "power": 2, //1-10 scale
            "speed": 6
        } //blah, blah, blah
    };
    this.weaponChoice = ["proca", "pistol", "ckm", "shotgun","bazooka"]; //TODO: Popraw procę i ckm (język) -.-
};

Weapon.prototype.preload = function () {
    //TODO: Załadować jako spritesheety, bo przecież broń jest widoczna z obu profilów (lewy i prawy)
    this.game.load.image('bazooka','assets/img/weapon/bazuka.png');
    this.game.load.image('ckm','assets/img/weapon/ckm.png');
    this.game.load.image('pistol','assets/img/weapon/pistolet.png');
    this.game.load.image('proca','assets/img/weapon/proca.png');
    this.game.load.image('shotgun','assets/img/weapon/strzelba.png');

    this.bullet.preload();
};

Weapon.prototype.create = function() {
    this.weaponSprite = this.game.add.sprite(this.x, this.y, this.weaponChoice[0]);
};

Weapon.prototype.update = function(x, y, isLeft) {

    this.weaponSprite.x = x;
    this.weaponSprite.y = y;
    if(!isLeft) {
        this.weaponSprite.x += this.playerWidth;
        //Zmień obrazek na "prawy"
    }
};

Weapon.prototype.fire = function () {
    //TODO: rozróżnienie bulletów w zależności od broni, tj. bazuka ma inny pocisk, a proca inny

    //TODO: obrażenia
    //new Bullet(this.game, this.x, this.y, 100); //Zahardkodowane
    this.bullet.create(this.weaponSprite.x, this.weaponSprite.y, 100); //Zahardkodowane
};

//Każdy powerup będzie miało swoje id, dzięki temu moglibyśmy identyfikować co z czym kolidowało