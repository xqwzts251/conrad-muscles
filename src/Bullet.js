/**
 * Created by Konrad on 2015-02-21.
 */
function Bullet(game) {
    this.game = game;
    //TODO: Rozróżnianie prędkości
    this.speed = 300;
};

Bullet.prototype.preload = function() {
    this.game.load.image("bullet", "assets/img/bullet/naboj.png"); //Tutaj trzeba uwzględnić inne bronie
};

Bullet.prototype.create = function(x, y, pointsValue) {
    var bulletSprite = this.game.add.sprite(x, y,"bullet");

    bulletSprite.checkWorldBounds = true;

    this.game.physics.arcade.enable(bulletSprite);
    bulletSprite.outOfBoundsKill = true;
    bulletSprite.body.velocity.y = -this.speed;
};

//Poniższe chyba niepotrzebne
/*
Bullet.prototype.update = function() {
    //this.bulletSprite.outOfBoundsKill = true; //<-- TODO: sprawdzić czy można dać tutaj czy w create()
    this.bulletSprite.body.velocity.y = -this.speed;
};
    */