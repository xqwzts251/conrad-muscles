/**
 * Created by Konrad on 2015-02-21.
 */
function MenuState(game) {
    this.game = game;
}

MenuState.prototype.preload = function() {
    this.game.load.image('background', 'assets/img/background/tlo.png');
    //this.game.load.image('floorBackground', 'assets/img/background/podloga.png');
    this.game.load.image('startButton', 'assets/img/menu/start.png');
};

MenuState.prototype.create = function() {
    var self = this; //ugly
    this.game.add.sprite(0,0, 'background');
    this.game.add.button(50, 150, 'startButton', function() {self.game.state.start('game');});
};